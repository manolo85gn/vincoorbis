<%--
  Created by IntelliJ IDEA.
  User: manolo
  Date: 3/22/13
  Time: 11:39 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>
</head>
<body>
    <g:render template="../regresar"/>
    <g:if test="${flash.message}">
        <br/>
        <div class="message" role="status">
            ${flash.message}
        </div>
        <br/>
    </g:if>

    <fileuploader:form	upload="docs"
                          successAction="success"
                          successController="archivo"
                          errorAction="error"
                          errorController="archivo"/>
</body>
</html>