<%--
  Created by IntelliJ IDEA.
  User: manolo
  Date: 3/22/13
  Time: 7:00 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>
</head>
<body>
    <g:render template="../regresar"/>
    <g:form action="concatenaCadenas" name="concatenaForm">
        <div class="fieldcontain">
            <label for="cadena1">
                Cadena 1
            </label>
            <g:textField name="cadena1" maxlength="40"/>
        </div>
        <div class="fieldcontain">
            <label for="cadena2">
                Cadena 2
            </label>
            <g:textField name="cadena2" maxlength="40"/>
        </div>
        <div style="margin-left: 250px; margin-top: 20px">
            <g:submitButton name="Concatenar"/>
        </div>
    </g:form>
    <div style="margin-left: 200px; margin-top: 20px">
        <p>Resultado: ${resultado?:''}</p>
    </div>
</body>
</html>