<%--
  Created by IntelliJ IDEA.
  User: manolo
  Date: 3/23/13
  Time: 2:21 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>
</head>
<body>
    <g:render template="../regresar"/>
    <g:form action="convertir" name="mayusculasForm">
        <div class="fieldcontain">
            <label for="cadena">
                Cadena
            </label>
            <g:textField name="cadena" maxlength="40"/>
        </div>

        <div style="margin-left: 250px; margin-top: 20px">
            <g:submitButton name="Convertir a mayúsculas"/>
        </div>
    </g:form>
</body>
</html>