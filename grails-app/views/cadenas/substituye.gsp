<%--
  Created by IntelliJ IDEA.
  User: manolo
  Date: 3/22/13
  Time: 7:19 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>
</head>
<body>
    <g:render template="../regresar"/>
    <g:form action="substituye" name="concatenaForm">
        <div class="fieldcontain">
            <label for="cadena">
                Cadena
            </label>
            <g:textField name="cadena" maxlength="40"/>
        </div>
        <div class="fieldcontain">
            <label for="caracter">
                Caracter
            </label>
            <g:textField name="caracter" maxlength="1"/>
        </div>
        <div style="margin-left: 250px; margin-top: 20px">
            <g:submitButton name="Substituir Caracter"/>
        </div>
    </g:form>
    <div style="margin-left: 200px; margin-top: 20px">
        <p>Resultado: ${resultado?:''}</p>
    </div>
</body>
</html>