<%--
  Created by IntelliJ IDEA.
  User: manolo
  Date: 3/23/13
  Time: 4:27 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>
</head>
<body>
    <g:render template="../regresar"/>
    <h1>Factorial</h1>
    <g:form action="calculaFactorial" name="calculaFactorialForm">
        <div class="fieldcontain">
            <label for="numero">
                Ingresa un número
            </label>
            <g:textField name="numero" maxlength="3"/>
        </div>

        <div style="margin-left: 250px; margin-top: 20px">
            <g:submitButton name="Calcula Factorial"/>
        </div>
    </g:form>
    <div style="margin-left: 200px; margin-top: 20px">
        <p>Resultado: ${resultado?:''}</p>
    </div>
</body>
</html>