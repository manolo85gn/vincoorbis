<%--
  Created by IntelliJ IDEA.
  User: manolo
  Date: 3/23/13
  Time: 2:57 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>
</head>
<body>
    <g:render template="../regresar"/>
    <h1>Matriz</h1>
    <table>
        <g:each in="${matriz}" var="row">
             <tr>
                 <g:each in="${row}" var="column">
                     <td>${column}</td>
                 </g:each>
             </tr>
        </g:each>
    </table>
    <g:form action="buscarOcurrencias" name="busquedaOcurrenciasForm">
        <div class="fieldcontain">
            <label for="cadena">
                Ingresa un número del 0 al 9
            </label>
            <g:textField name="cadena" maxlength="1"/>
        </div>

        <div style="margin-left: 250px; margin-top: 20px">
            <g:submitButton name="Buscar Ocurrencias"/>
        </div>
    </g:form>
    <div style="margin-left: 200px; margin-top: 20px">
        <p>Número de Ocurrencias: ${resultado?:''}</p>
    </div>
</body>
</html>