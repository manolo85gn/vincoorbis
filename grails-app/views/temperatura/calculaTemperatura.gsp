<%--
  Created by IntelliJ IDEA.
  User: manolo
  Date: 3/22/13
  Time: 3:32 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main"/>
    <title></title>
    <r:require module="jquery"/>
</head>
<body>
    <g:render template="../regresar"/>
    <h1>Calculadora de temperaturas</h1>

    <g:formRemote name="calculaTemperatura" update="resultadoId" url="[action: 'calculaTemperatura']" asynchronous="true">

        <div style="margin-top: 20px">
            <label style="margin-left: 10px; font: bold">Ingresa un valor</label>
            <g:textField name="numero" value="0"/>
        </div>

        <p style="margin-left: 10px; font: bold">Convertir de: </p>
        <div style="margin-left: 100px">
            <g:radioGroup values="['degreeCelsius','degreeFahrenheit','kelvin']" name="gradosDe" value="degreeCelsius"
                    labels="['Celsius','Fahrenheit','Kelvin']">
                <p>${it.radio} <g:message code="${it.label}" /></p>
            </g:radioGroup>
        </div>

        <p style="margin-left: 10px; font: bold">a: </p>
        <div style="margin-left: 100px">
            <g:radioGroup values="['degreeCelsius','degreeFahrenheit','kelvin']" name="gradosA" value="degreeFahrenheit"
                          labels="['Celsius','Fahrenheit','Kelvin']">
                <p>${it.radio} <g:message code="${it.label}" /></p>
            </g:radioGroup>
        </div>

        <a style="margin-left: 200px" href="#" class="last buttons" onclick="$('#calculaTemperatura').submit()">Calcular</a>
    </g:formRemote>

    <div id="resultadoId" style="margin-left: 150px; margin-top: 50px">
        <g:render template="resultado"/>
    </div>
</body>
</html>