<%--
  Created by IntelliJ IDEA.
  User: manolo
  Date: 3/23/13
  Time: 4:47 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>
</head>
<body>
    <g:render template="../regresar"/>
    <h1>Validación dato numérico</h1>
    <g:if test="${flash.message}">
        <br/>
        <div class="message" role="status">
            ${flash.message}
        </div>
        <br/>
    </g:if>
    <g:form action="validacion" name="validacionForm">
        <div class="fieldcontain ${hasErrors(bean:input,field:'dato','errors')}">
            <label for="dato">
                Ingresa un número
            </label>
            <g:textField name="dato" maxlength="30" value="${fieldValue(bean:input,field:'dato')}"/>
        </div>

        <div style="margin-left: 250px; margin-top: 20px">
            <g:submitButton name="Envía"/>
        </div>
    </g:form>

</body>
</html>