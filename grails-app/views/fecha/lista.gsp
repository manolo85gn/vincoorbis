<%--
  Created by IntelliJ IDEA.
  User: manolo
  Date: 3/22/13
  Time: 5:45 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>
</head>
<body>
    <g:render template="../regresar"/>
    <h1>Fechas</h1>
    <div style="margin-left: 200px">
        <table>
            <g:each in="${fechas}" var="item">
                <tr>
                    <td>${item.label}</td>
                    <td>${item.fecha}</td>
                </tr>
            </g:each>
        </table>
    </div>

</body>
</html>