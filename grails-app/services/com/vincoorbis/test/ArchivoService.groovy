package com.vincoorbis.test

class ArchivoService {


    def validaLinea(String linea) {
        def tokens = linea?.split(';')
        tokens &&
            tokens.length == 3 &&
            validaNumeroUsuario(tokens[0]) &&
            validaMonedasAsociadas(tokens[1]) &&
            validaNombreUsuario(tokens[2])
    }

    def validaNumeroUsuario(String numero) {
        numero && numero.length() == 10 && numero.isInteger()
    }

    def validaMonedasAsociadas(String numero) {
        numero && numero.isInteger()
    }

    def validaNombreUsuario(String nombre) {
        nombre && nombre.trim() != ""
    }

    Integer validaArchivo(String path) {
        def file = new File(path)
        def count = 0
        if(file.exists()) {
            def lines = file.readLines()
            for(it in lines) {
                count ++
                if(!validaLinea(it)) {
                    return count
                }
            }
        }

        -1
    }
}
