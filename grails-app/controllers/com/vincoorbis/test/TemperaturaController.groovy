package com.vincoorbis.test

import org.grails.plugins.wsclient.service.WebService

class TemperaturaController {

    WebService webService

    def index() {
        render view: 'calculaTemperatura'
    }

    def calculaTemperatura() {
        def wsdlURL = "http://www.webservicex.net/ConvertTemperature.asmx?WSDL"
        def proxy = webService.getClient(wsdlURL)
        def result = proxy.ConvertTemp(params.numero, params.gradosDe, params.gradosA)
        render template: 'resultado', model: [resultado: result]
    }
}
