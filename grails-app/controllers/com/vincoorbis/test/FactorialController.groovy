package com.vincoorbis.test

class FactorialController {

    def index() {  render(view: 'calculaFactorial')}

    def calculaFactorial() {
        def resultado = 0
        String numero = params.numero
        if (numero && numero.isInteger() ) {
            resultado = factor(numero.toInteger())
        }
        [resultado: resultado]
    }

    private factor(Integer numero) {
        numero == 0 ? 1 : numero * factor(numero-1)
    }
}
