package com.vincoorbis.test

import org.joda.time.DateTime

class CadenasController {

    def index() {
        render view: 'concatenaCadenas'
    }

    def concatenaCadenas() {
        def resultado = params.cadena1 + params.cadena2
        [resultado: resultado]
    }

    def substituye() {
        String cadena = params.cadena
        def caracter = params.caracter
        [resultado: cadena?.replace(caracter, '')]
    }

    def mayusculas() {}

    def convertir() {
        String cadena = params.cadena
        cadena = cadena.toUpperCase()
        response.setContentType("text/txt")
        response.setHeader("Content-disposition", "attachment; filename=prueba-${DateTime.now()}.txt")
        response.outputStream << cadena.getBytes()
        response.outputStream.flush()
    }
}
