package com.vincoorbis.test

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormatter
import org.joda.time.format.DateTimeFormat

class FechaController {

    def index() {
        def formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm")
        def actual = DateTime.now()
        def primerDiaMesActual = actual.dayOfMonth().withMinimumValue().withHourOfDay(0).withMinuteOfHour(0)

        def ultimoDiaMesActual = actual.dayOfMonth().withMaximumValue().withHourOfDay(23).withMinuteOfHour(59)

        def fechas = [ new Item(label: 'Primer día mes actual', fecha: formatter.print(primerDiaMesActual) ),
                new Item(label: 'Día actual', fecha: formatter.print(actual) ),
                new Item(label: 'Último día mes actual', fecha: formatter.print(ultimoDiaMesActual) )]

        render(view: 'lista', model: [fechas: fechas])
    }


}

class Item {
    def label
    def fecha
}




















