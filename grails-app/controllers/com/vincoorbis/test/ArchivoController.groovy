package com.vincoorbis.test

import com.lucastex.grails.fileuploader.UFile

class ArchivoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def archivoService

    def index() {
        render(view: "uploadForm", params: params)
    }

    def success() {
        def ufile = UFile.get(params.ufileId)
        def resultado = archivoService.validaArchivo(ufile.path)
        if (resultado == -1) {
            flash.message = message([code: "archivo.respuesta.exito"])
        } else {
            flash.message = message([code: "archivo.respuesta.error", args: [resultado]])
            ufile.delete()
        }

        render view: 'uploadForm'
    }

    def error() {
        flash.message = message([code: "archivo.error.inesperado"])
        render view: 'uploadForm'
    }

}
