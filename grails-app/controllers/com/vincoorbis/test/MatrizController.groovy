package com.vincoorbis.test

class MatrizController {

    def matriz = [[1, 2, 3, 4, 5],
                  [6, 7, 8, 9, 1],
                  [9, 6, 8, 2, 1],
                  [3, 6, 1, 4, 8],
                  [2, 1, 5, 3, 4],
                  [0, 8, 1, 3, 5],
                  [6, 7, 8, 9, 2]]

    def index() {
        render( view: 'buscarOcurrencias', model: [matriz: matriz])
    }

    def buscarOcurrencias() {
        def resultado = 0
        String cadena = params.cadena
        def numero = (cadena?.isInteger())? cadena.toInteger() : null
        if (numero) {
            matriz.each { row ->
                row.each {
                   if( it == numero) {
                       resultado++
                   }
                }
            }
        }

        [resultado: resultado, matriz: matriz]
    }
}
