package com.vincoorbis.test

class NumericoController {

    def index() { }

    def validacion = { InputCommand input ->
        if ( input.validate() ){
            flash.message = "Valor correcto: ${params.dato}"
        } else {
            flash.message = "El valor ${params.dato} no cumple el formato"
        }
        render( view: 'formulario')
    }

    def formulario(){
        println params
        def input = params.input ?: new InputCommand()
        [input: input]
    }

}

class InputCommand {
    Integer dato

    static constraints = {
         dato(nullable: false, blank:false)
    }
}
