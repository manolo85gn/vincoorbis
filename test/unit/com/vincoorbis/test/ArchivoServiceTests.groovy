package com.vincoorbis.test



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ArchivoService)
class ArchivoServiceTests {

    void testValidaArchivo() {
        String contenido = "0000012345;1;manuel1\n0000012346;2;manuel2\nx00001234x;3;manuel3\n0000012348;4;manuel4"
        def path = "/tmp/prueba.txt"
        def file = new File(path)
        if(file.exists()) {
            file.delete()
        }
        file.createNewFile()
        file.append(contenido)
        assert service.validaArchivo(path) == 3
    }

    void testLinea() {
        String linea = "0000012345;2;manuel"
        assert service.validaLinea(linea)
        linea = "0000000012345;2;manuel"
        assert !service.validaLinea(linea)
        linea = "0000012345;2.1;manuel"
        assert !service.validaLinea(linea)
        linea = "0000012345;2;"
        assert !service.validaLinea(linea)
        linea = ""
        assert !service.validaLinea(linea)
        linea = null
        assert !service.validaLinea(linea)
    }

    void testvalidaNumeroUsuario() {
        String numero = "0000012345"
        assert service.validaNumeroUsuario(numero)
        numero = "1234567890"
        assert service.validaNumeroUsuario(numero)
        numero = "0000x12345"
        assert !service.validaNumeroUsuario(numero)
        numero = "12345"
        assert !service.validaNumeroUsuario(numero)
        numero = ""
        assert !service.validaNumeroUsuario(numero)
        numero = null
        assert !service.validaNumeroUsuario(numero)
    }

    void testMonedasAsociadas() {
        String numero = "123"
        assert service.validaMonedasAsociadas(numero)
        numero = "012"
        assert service.validaMonedasAsociadas(numero)
        numero = "120.2"
        assert !service.validaMonedasAsociadas(numero)
        numero = "12.00"
        assert !service.validaMonedasAsociadas(numero)
        numero = ""
        assert !service.validaMonedasAsociadas(numero)
        numero = null
        assert !service.validaMonedasAsociadas(numero)
    }

    void testNombreUsuario() {
        String nombre = "manuel"
        assert service.validaNombreUsuario(nombre)
        nombre = ""
        assert !service.validaNombreUsuario(nombre)
        nombre = "    "
        assert !service.validaNombreUsuario(nombre)
        nombre = null
        assert !service.validaNombreUsuario(nombre)
    }
}
